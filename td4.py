import requests
import re
from bs4 import BeautifulSoup
import csv


#Question 3 #########################################################################################################################


def recupInfosQuestion(question):
    """
    Retourne un tuple contenant le titre, les votes et les réponses de la question passé en paramètre
    """

    titre = str(question.h3.a.text)
    votes = int(question.find_all('div',class_="votes")[0].span.text)
    reponses = int(question.find_all('div',class_="status")[0].span.text)

    return (titre, votes, reponses)

def export_Infos_Questions_CSV():
    """
    Insertion des informations (titre,vues,réponses) correpondantes à chaque question dans un fichier CSV (créée ou utilisé)
    """
    UNI = requests.get("https://stackoverflow.com/")
    soup = BeautifulSoup(UNI.text, 'lxml')
    liste_questions = (soup.find_all('div',class_="question-summary narrow"))[:10]
    liste_infos_question = [recupInfosQuestion(question) for question in liste_questions]


    file = open("question3.csv", "w") # ouverture ou création d'un fichier CSV
    try:
        writer = csv.writer(file)
        writer.writerow(("Titre","Vues","Réponses")) # on insère le nom des colonnes
        for ((titre,vues,colonnes)) in liste_infos_question:
            writer.writerow((titre,vues,colonnes)) # pour chaque question on insére les informations dans le CSV
    finally:
        file.close()

print(export_Infos_Questions_CSV())

#Question 4 #########################################################################################################################

def recup_licences_pro():
    """
    Retourne la liste des formations de type Licence Pro offertes par l'université d'Orléans
    """

    URL = "http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav"
    data = {"degree": 'DP',"submit-form":""} # 'degree' pour le diplôme et 'submit-form''pour envoyer la réponse du formulaire
    session = requests.session()
    r = requests.post(URL, data=data ) # récupère les données de la page web après envoi du formulaire avec "Licence professionnelle" sélectionné pour le diplôme
    soup = BeautifulSoup(r.text, 'lxml')
    liste_formations = (soup.find_all('li',class_="hit"))

    return [formation.find_all("p")[0].strong.text for formation in liste_formations] # liste contenant les titres des licences professionnelles

print(recup_licences_pro())

#Question 5 #########################################################################################################################

def get_definition1(x):
    """
    Get Word definition from aonaware Site
    """
    URL = 'http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'.format(x)
    html = requests.get(URL).text
    soup = BeautifulSoup(html, 'lxml')
    return soup.find('pre').text

print(get_definition1("hello"))


def get_definition2(x):
    """
    Get Word definition from aonaware Site
    """
    URL = 'http://services.aonaware.com/DictService/Default.aspx'
    data = {'action':'define','dict':'wn','query':x}
    html = requests.get(URL,params=data).text
    soup = BeautifulSoup(html,'lxml')
    return soup.find('pre').text

print(get_definition2("hello"))


def get_definition_file(file):
    lignes=[]
    with open(file,'r') as vocabulaire:
        for ligne in vocabulaire.readlines():
            lignes.append(ligne.replace('\n',''))
    vocabulaire.close()
    with open('definition.txt','w') as dictionnaire:
        for mot in lignes :
            dictionnaire.write(get_definition2(mot))
            dictionnaire.write('\n')
    dictionnaire.close()

get_definition_file('vocabulary.txt')
